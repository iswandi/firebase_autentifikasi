package com.blackswan.autentifikasifirebase.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.blackswan.autentifikasifirebase.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChangeemailFragment extends Fragment {


    public ChangeemailFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_changeemail, container, false);
    }

}
