package com.blackswan.autentifikasifirebase.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.blackswan.autentifikasifirebase.MainActivity;
import com.blackswan.autentifikasifirebase.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class LoginActivity extends AppCompatActivity {

    FirebaseAuth auth;
    EditText email, password;
    ProgressBar pb;
    FirebaseAuth.AuthStateListener listener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        email = (EditText) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.password);
        pb = (ProgressBar) findViewById(R.id.progressBar);
        auth = FirebaseAuth.getInstance();

        if (auth.getCurrentUser() !=null){
            startActivity(new Intent(LoginActivity.this, MainActivity.class));
        finish();
        }


//        listener = new FirebaseAuth.AuthStateListener() {
//            @Override
//            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
//                FirebaseUser user = auth.getCurrentUser();
//                if (user != null) {
//                    Toast.makeText(LoginActivity.this,
//                            "user telah terdaftar,silahkan cek email untuk verifikasi",
//                            Toast.LENGTH_SHORT).show();
//                }
//            }
//        };


    }

    public void onKlikRegister(View view) {
        startActivity(new Intent(LoginActivity.this, Register_activity.class));

    }

    public void onKlikForgot(View view) {
        startActivity(new Intent(LoginActivity.this, ResetPAsswordActivity.class));

    }

    public void onKlikLogin(View view) {
        String e = email.getText().toString();
        String p = password.getText().toString();
        if (TextUtils.isEmpty(e)) {
            email.setError("email tidak boleh kosong");
            Animation getar = AnimationUtils.loadAnimation(
                    LoginActivity.this, R.anim.animasigetar);
            email.startAnimation(getar);

        } else if (TextUtils.isEmpty(p)) {
            password.setError("password tidak boleh kosong");
            Animation getar = AnimationUtils.loadAnimation(
                    LoginActivity.this, R.anim.animasigetar);
            password.startAnimation(getar);

        } else if (p.length() < 6) {
            password.setError(getString(R.string.minimum_password));
            Animation getar = AnimationUtils.loadAnimation(
                    LoginActivity.this, R.anim.animasigetar);
            password.startAnimation(getar);

        } else {
            pb.setVisibility(View.VISIBLE);

            auth.signInWithEmailAndPassword(e, p).addOnCompleteListener(LoginActivity.this, new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    pb.setVisibility(View.GONE);
                    if (!task.isSuccessful()) {
                        Toast.makeText(LoginActivity.this, getString(R.string.auth_failed)
                                + task.getException(), Toast.LENGTH_SHORT).show();
                    } else {
                        startActivity(new Intent(LoginActivity.this, MainActivity.class));
                        finish();
                    }

                }
            });
        }
    }

//    @Override
//    protected void onStart() {
//        auth.addAuthStateListener(listener);
//        super.onStart();
//    }
//
//    @Override
//    protected void onStop() {
//        super.onStop();
//        if (listener != null) {
//            auth.removeAuthStateListener(listener);
//        }
//    }
}